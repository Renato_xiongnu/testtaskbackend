﻿namespace TestTask.Repository.Interfaces
{
    public interface IRepositoryWrapper
    {
        IPersonRepository PersonRepository { get; }



        void Save();
    }
}
