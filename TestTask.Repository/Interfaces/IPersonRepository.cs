﻿using TestTask.Domain.Models;

namespace TestTask.Repository.Interfaces
{
    public interface IPersonRepository : IRepositoryBase<Person>
    {
    }
}
