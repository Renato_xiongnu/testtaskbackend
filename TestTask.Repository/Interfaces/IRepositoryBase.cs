﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TestTask.Repository.Interfaces
{
    public interface IRepositoryBase<T>
    {

        #region Non Async

        IQueryable<T> GetAll();

        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);

        void Create(T entity);

        void Update(T entity);

        void Delete(T entity);

        #endregion


        #region Async

        Task<ICollection<T>> GetAllAsync();

        Task<ICollection<T>> FindByConditionAsync(Expression<Func<T, bool>> expression);

        Task<bool> CreateAsync(T entity);

        Task<bool> UpdateAsync(T entity);

        Task DeleteAsync(T entity);

        #endregion

    }
}
