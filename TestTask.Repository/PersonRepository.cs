﻿using TestTask.Domain;
using TestTask.Domain.Models;
using TestTask.Repository.Interfaces;

namespace TestTask.Repository
{
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        public PersonRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
