﻿using Microsoft.EntityFrameworkCore;
using TestTask.Domain.Models;

namespace TestTask.Domain
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }

   

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
