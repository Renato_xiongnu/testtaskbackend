﻿using AutoMapper;
using TestTask.Domain.Models;

namespace TestTask.Api.ViewModels.Mapping
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<PersonViewModel, Person>();
        }
    }
}
