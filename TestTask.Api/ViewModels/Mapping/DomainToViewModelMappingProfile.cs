﻿using AutoMapper;
using TestTask.Domain.Models;

namespace TestTask.Api.ViewModels.Mapping
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Person, PersonViewModel>();
        }
    }
}
