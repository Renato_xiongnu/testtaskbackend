﻿using FluentValidation;

namespace TestTask.Api.ViewModels.Validation
{
    public class PersonViewModelValidator : AbstractValidator<PersonViewModel>
    {
        public PersonViewModelValidator()
        {
            RuleFor(p => p.FirstName).NotEmpty().WithMessage("First Name cannot be empty");
            RuleFor(p => p.LastName).NotEmpty().WithMessage("Last Name cannot be empty");
            RuleFor(p => p.Email).NotEmpty().WithMessage("Email cannot be empty");
            RuleFor(p => p.Phone).NotEmpty().WithMessage("Phone cannot be empty");
            RuleFor(p => p.Reason).NotEmpty().WithMessage("Reason cannot be empty");
            RuleFor(p => p.Password).NotEmpty().WithMessage("Password cannot be empty");

        }
    }
}
