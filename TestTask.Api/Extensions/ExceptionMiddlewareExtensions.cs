﻿using Microsoft.AspNetCore.Builder;
using TestTask.Api.CustomExceptionMiddleware;


namespace TestTask.Api.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
