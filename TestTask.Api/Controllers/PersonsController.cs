﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Api.Controllers.Helper;
using TestTask.Api.ViewModels;
using TestTask.Domain.Models;
using TestTask.Repository.Interfaces;

namespace TestTask.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly ILogger<PersonsController> _logger;
        private readonly IRepositoryWrapper _repo;


        public PersonsController(
                                IRepositoryWrapper repo,
                                ILogger<PersonsController> logger)
        {
            _repo = repo;
            _logger = logger;
        }


        [HttpGet]
        [Route("GetPersons")]
        public async Task<IActionResult> GetPersons()
        {
            var persons = await _repo.PersonRepository.GetAllAsync();
            IEnumerable<PersonViewModel> personsVM = Mapper.Map<IEnumerable<Person>, IEnumerable<PersonViewModel>>(persons);

            return Ok(personsVM);
        }


        [HttpGet]
        [Route("GetPersonsById/{id}")]
        public async Task<IActionResult> GetPersonById(int id)
        {
            var person = await _repo.PersonRepository.FindByConditionAsync(p => p.Id == id);
            PersonViewModel personVM = Mapper.Map<Person, PersonViewModel>(person.FirstOrDefault());

            return Ok(personVM);
        }



        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreatePerson([FromBody] PersonViewModel personVM)
        {
            bool isSuccess = false;

            var personsInDb = await _repo.PersonRepository.FindByConditionAsync(p => p.Email.Equals(personVM.Email));

            if (personsInDb.Count > 0)
            {
                var currentPerson = personsInDb.First();
                currentPerson.IsReregistrating = true;
                string reason = "Trying to re-register";

                bool isUpdated = await _repo.PersonRepository.UpdateAsync(currentPerson);

                if (!isUpdated)
                {
                    reason = "Error occured while trying to update the DB for already registered person";
                }

                return Ok(new { Status = isSuccess, Reason = reason });
            }

            Person person = Mapper.Map<PersonViewModel, Person>(personVM);
            person.IsReregistrating = false;
            person.Password = PasswordHelper.GeneratePassword();

            var res = await _repo.PersonRepository.CreateAsync(person);

            return Ok(new { Status = res, Reason = "All is good", Password = person.Password });
        }


    }
}